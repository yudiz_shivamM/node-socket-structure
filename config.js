const dev = {
    PORT: process.env.PORT || 3000,
}
const prod = {
    PORT: process.env.PORT || 80,
}

module.exports = (process.env.NODE_ENV === 'prod') ? prod : dev;