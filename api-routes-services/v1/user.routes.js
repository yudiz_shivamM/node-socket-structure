const UserRouter = require('express').Router();
const isUserAuthenticated = require('./middlewares/auth.middlewares').isUserAuthenticated;
const services = require('./user.services');

UserRouter.use(isUserAuthenticated);

UserRouter.get('/profile', services.GetProfile);
UserRouter.post('/logout', services.Logout);

module.exports = UserRouter;