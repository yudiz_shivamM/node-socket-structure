const router = require('express').Router();
const services = require('./admin.services');
const isAdminAuthenticated = require('./middlewares/auth.middlewares').isAdminAuthenticated;

router.use(isAdminAuthenticated);

router.get('/profile', services.GetProfile);
router.put('/profile', services.EditProfile);
router.post('/changePassword', services.ChangePassword);

router.get('/statistics', services.GetStatistics);

router.get('/listUsers', services.GetUserList);

router.get('/user', services.GetUser);
router.put('/user', services.EditUser);
router.delete('/user', services.DeleteUser);

router.get('/siteSetting', services.GetSiteSetting);
router.put('/siteSetting', services.EditSiteSetting);

router.post('/logout', services.Logout);

module.exports = router;