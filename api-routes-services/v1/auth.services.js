const _ = require('lodash');
const usersModel = require('../../models/users.model');
const messageService = require('../../api.response').userMessages;

/**
 * User Routes
 */
const UserLogin = (req, res) => {
    res.send("UserLogin");
}
const UserRegistration = (req, res) => {
    res.send("UserRegistration");
}
const UserForgotPassword = (req, res) => {
    res.send("UserForgotPassword");
}
const UserResetPassword = (req, res) => {
    res.send("UserResetPassword");
}

/**
 * Admin Routes
 */
const AdminLogin = (req, res) => {
    res.send("AdminLogin");
}
const AdminRegistration = (req, res) => {
    res.send("AdminRegistration");
}
const AdminForgotPassword = (req, res) => {
    res.send("AdminForgotPassword");
}
const AdminResetPassword = (req, res) => {
    res.send("UserResetPassword");
}

module.exports = {
    UserLogin,
    UserRegistration,
    UserForgotPassword,
    UserResetPassword,
    AdminLogin,
    AdminRegistration,
    AdminForgotPassword,
    AdminResetPassword
}