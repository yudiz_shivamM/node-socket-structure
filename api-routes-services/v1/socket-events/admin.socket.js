const isSocketAuthenticated = require('./../middlewares/auth.middlewares').isAdminSocketAuthenticated;
const services = require('./../socket-events/admin.socket.services');
module.exports = (io) => {

    //Enable cross domain access from evry wahere
    io.origins('*:*');

    //Middleware to authenticate user   
    io.use(isSocketAuthenticated);

    io.on('connection', (socket) => {
        console.log(socket.id, "is connected");

        //Example of `emit` Event
        socket.emit('resOfExampleEvent', { data: "Example data" });
        /*
            Example of `On` Event
            Do not pass io, socket parameter untill it is necessary.
        */
        socket.on('reqExampleEvent', (data,callback) => services.ExampleService(io,socket,data,callback));

        socket.on('disconnect',(data,callback) => services.Disconnected(socket));
    });
}