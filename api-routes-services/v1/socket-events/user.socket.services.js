const ExampleService = (io,socket,data,callback) => {
    callback("ExampleServiceCalled");
}

const Disconnected = (socket) => {
    console.log(socket.id," : Disconnected");
}

module.exports = {
    ExampleService,
    Disconnected
}