const router = require('express').Router();
const services = require('./auth.services');

router.post('/user/login', services.UserLogin);
router.post('/user/registeration', services.UserRegistration);
router.post('/user/forgot_password', services.UserForgotPassword);
router.post('/user/reset_password', services.UserResetPassword);

router.post('/admin/login', services.AdminLogin);
router.post('/admin/register', services.AdminRegistration);
router.post('/admin/forgot_password', services.AdminForgotPassword);
router.post('/admin/reset_password', services.AdminResetPassword);

module.exports = router;