const _ = require('lodash');

const mstPreferenceModel = require('../../models/mst.preference.model');
const usersModel = require('../../models/users.model');
const messageService = require('../../api.response').adminMessages;

const GetProfile = (req, res) => {
    res.send("GetProfile");
}
const EditProfile = (req, res) => {
    res.send("EditProfile");
}
const ChangePassword = (req, res) => {
    res.send("ChangePassword");
}
const GetStatistics = (req, res) => {
    res.send("GetStatistics");
}
const GetUserList = (req, res) => {
    res.send("GetUserList");
}
const GetUser = (req, res) => {
    res.send("GetUser");
}
const EditUser = (req, res) => {
    res.send("EditUser");
}
const DeleteUser = (req, res) => {
    res.send("DeleteUser");
}
const GetSiteSetting = (req, res) => {
    res.send("GetSiteSetting");
}
const EditSiteSetting = (req, res) => {
    res.send("EditSiteSetting");
}
const Logout = (req, res) => {
    res.send("Logout");
}

module.exports = {
    GetProfile,
    EditProfile,
    ChangePassword,
    GetStatistics,
    GetUserList,
    GetUser,
    EditUser,
    DeleteUser,
    GetSiteSetting,
    EditSiteSetting,
    Logout
}