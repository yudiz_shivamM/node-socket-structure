const _ = require('lodash');

const mstPreferenceModel = require('../../models/mst.preference.model');
const usersModel = require('../../models/users.model');
const messageService = require('../../api.response').adminMessages;

const GetProfile = (req, res) => {
    res.send("GetProfile");
}

const Logout = (req, res) => {
    res.send("Logout");
}

module.exports = {
    GetProfile,
    Logout
}