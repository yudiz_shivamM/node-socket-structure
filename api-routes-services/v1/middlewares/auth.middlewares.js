const isUserAuthenticated = (req, res, next) => {
    next();
}
const isAdminAuthenticated = (req, res, next) => {
    next();
}
const isSuperAdminAuthenticated = (req, res, next) => {
    next();
}

/**
 * User Socket Authnetication
 */

const isUserSocketAuthenticated = (socket, next) => {
    //Socket Header will pass from the front-end in `Query` or headers
    var token = socket.handshake.headers.query || socket.handshake.query.token;
}

/**
 * Admin Socket Authnetication
 */

const isAdminSocketAuthenticated = (socket, next) => {
    //Socket Header will pass from the front-end in `Query` or headers
    var token = socket.handshake.headers.query || socket.handshake.query.token;
}

module.exports = {
    isUserAuthenticated,
    isAdminAuthenticated,
    isSuperAdminAuthenticated,
    isUserSocketAuthenticated,
    isAdminSocketAuthenticated
}