/**
 *  Dependency declaration
 */
const app = require('express')();
const server = require('http').Server(app);
const bodyParser = require('body-parser');
const io = require('socket.io')(server);
/**
 *  Routes and configuration environment initialization
 */
const config = require('./config');
const authRoute = require('./api-routes-services/v1/auth.routes');
const userRoute = require('./api-routes-services/v1/user.routes');
const adminRoute = require('./api-routes-services/v1/admin.routes');
/**
 *  Dependency Initialization
 */
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.header('Access-Control-Expose-Headers', 'Authorization');
    next();
});
/**
 *  Routes definations
 */
app.use('/api/v1/auth/', authRoute);
app.use('/api/v1/user/', userRoute);
app.use('/api/v1/admin/', adminRoute);
/**
 *  Socket events definations
 */
require('./api-routes-services/v1/socket-events/admin.socket')(io);
require('./api-routes-services/v1/socket-events/user.socket')(io);
/**
 *  Running Server
 */
server.listen(config.PORT, () => console.log(`Magic happens on port ${config.PORT}`));