const adminMessages = {
    USER_NOT_FOUND: "User Not Found"
}
const userMessages = {
    USER_NOT_FOUND: "User Not Found"
}

module.exports = {
    adminMessages,
    userMessages
}